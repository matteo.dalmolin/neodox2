FROM frolvlad/alpine-glibc:latest

ENV REFRESHED_AT 2024-12-17

# Set up a tools dev directory
WORKDIR /qmk_firmware

RUN apk add --no-cache --virtual build-dependencies \
    bzip2-dev \
    ca-certificates \
    openssl \
    tar \
    w3m \
    xz \
    && apk add --no-cache \
    python3 \
    py3-pip \
    git \
    make \
    cmake \
    nodejs \
    ninja \
    scons

RUN wget https://developer.arm.com/-/media/Files/downloads/gnu-rm/10-2020q4/gcc-arm-none-eabi-10-2020-q4-major-x86_64-linux.tar.bz2 \
    && tar xvf gcc-arm-none-eabi-10-2020-q4-major-x86_64-linux.tar.bz2 \
    && rm gcc-arm-none-eabi-10-2020-q4-major-x86_64-linux.tar.bz2 \
    && apk del build-dependencies \
    && rm -rf /home/dev/gcc-arm-none-eabi-10-2020-q4-major/share/doc

RUN git clone --recurse-submodules https://github.com/qmk/qmk_firmware.git \
    && cd qmk_firmware \
    && git checkout 0.27.1 \
    && git submodule update --recursive

RUN pip install --break-system-packages qmk
RUN pip install --break-system-packages keymap-drawer==0.15.0

# Set up the compiler path
ENV PATH="/qmk_firmware/gcc-arm-none-eabi-10-2020-q4-major/bin:${PATH}"

WORKDIR /qmk_firmware/qmk_firmware
