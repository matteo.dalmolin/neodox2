# Build Options
#   change yes to no to disable
#
RGBLIGHT_SUPPORTED = no    # RGB underglow is supported, but not enabled by default
SERIAL_DRIVER = vendor
QUANTUM_PAINTER_DRIVERS = gc9a01_spi
CAPS_WORD_ENABLE = yes
